import generateCode from "../helper/generateCode/index";



test('generate Code',()=>{
  const link ='https://github.com/leonidlebedev/javascript-airbnb'
  const reg = new RegExp('^[a-zA-Z0-9]')
  expect(generateCode(link)).toMatch(reg)
})