import getSite from '../controller/link/getSite'
import config from '../config'
import {initClient,getClient} from '../redis/index'

test('chek find link',async ()=>{
  initClient()
  const client = getClient()
  client.set('qwe11213','https://github.com/leonidlebedev/javascript-airbnb')
  const link = `${config.domen}qwe11213`
  expect(await getSite(link)).toBe('https://github.com/leonidlebedev/javascript-airbnb')
})