import createNewLink from "../controller/link/createNewLink";

test('create new link',()=>{
  const testLink='https://mean-dev.info/mean-stack-docker-part-5/'
  const reg = new RegExp('^[a-zA-Z0-9]')
  expect(createNewLink(testLink)).toMatch(reg)
})