import './app'
import './router/index'
import { waitConnect } from "./db/connect";
import { initClient } from './redis/index';

(async () => {
  try {
  await waitConnect
  initClient()
    
  } catch (error) {
    console.log(error)
  }
})()