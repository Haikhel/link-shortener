import dbManager from '../index'

import config from '../../config'

export default (dbName, object) => {
//  if (!dbManager.connected) await waitConnect
  const connect = dbManager.mongodbClient
  const collection = connect.db(config.MONGO_DB_NAME).collection(dbName)
  if (!object) {
    return
  }
  collection.insertOne(object)
}
