import dbManager from '../index'
import config from '../../config'



export default async (dbName) => {
  // if (!dbManager.connected) await waitConnect
  const connect = dbManager.mongodbClient
  const collection = connect.db(config.MONGO_DB_NAME).collection(dbName)
  const models = await collection.find().toArray()
  if (!models) {
    return null
  }
  return models
}
