import { client, waitConnect } from './connect'

let connected = false

waitConnect.then(() => {
  connected = true
})

export default {
  mongodbClient: client,
  connected,
  waitConnect,
}
