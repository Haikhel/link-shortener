import mongodb from 'mongodb'
import config from '../config';

export const client = new mongodb.MongoClient(config.MONGO_DB_URL, {
  wtimeout: 5000,
  useNewUrlParser: true,
  retryWrites: false,
  useUnifiedTopology: true,
  w: 'majority',
})

export const waitConnect = (async () => {
  const res = await client.connect()
  if (res) console.log('mongodb ~gl~connected')
  return client
})()
