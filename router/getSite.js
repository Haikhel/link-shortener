import app from '../app'
import getSite from '../controller/link/getSite'

app.get('/getSite',async (req,res)=>{
  const {link}=req.query
  if (!link) {
    res.status(400).send()
    return
  }
  const oldLink =await getSite(link)
  if (oldLink) {
    res.status(200).send({link:oldLink})
  }else{
    res.status(400).send()
  }
})