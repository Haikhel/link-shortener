import app from '../app'
import getSite from '../controller/link/getSite'

app.get('/:link',async (req,res)=>{
  const {link}=req.params
  if (!link) {
    res.status(400).send()
    return
  }
  const oldLink =await getSite(link)
  if (oldLink) {
    res.redirect(oldLink)
  }else{
    res.status(400).send()
  }
})