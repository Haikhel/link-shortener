import app from '../app'
import createNewLink from '../controller/link/createNewLink'

app.post('/generateLink',(req,res)=>{
  const {link}=req.body
  if (!link) {
    res.status(400).send()
    return
  }
  const newLink = createNewLink(link)
  if (newLink) {
    res.status(200).send({link:newLink})
  }else{
    res.status(400).send()
  }
})