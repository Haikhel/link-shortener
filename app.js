import express from 'express'
import bodyParser from 'body-parser'
import helmet from 'helmet'
import config from './config'


const app = express();

app.use(helmet()) 
app.use(bodyParser.urlencoded({ extended: true })) 
app.use(bodyParser.json()) 

app.listen(config.port, () => {
  console.log(`Server is up and running on port ${  config.port  }!`)
})

export default app