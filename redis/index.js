import redis from 'redis'
import dbName from '../db/const/dbName';
import getAll from '../db/controller/getAll';

let client=null
export const initClient = ()=> {
   client = redis.createClient({port:6379})
   client.on("error", (error) => {
    console.log('error')
    console.error(error);
  });
  client.on("connect", async () => {
    console.log('connect')
    const linkList=await getAll(dbName.shortLink)
    for (let i = 0; i < linkList.length; i+=1) {
      client.set(linkList[i].code,linkList[i].oldLink)
      console.log(client.get(linkList[i].code))
    }
  });
}

export const getValueByKey = (key)=>new Promise((resv, rej) => {
    client.get(key, (err, reply) => {
      if (err) {
        rej(err)
      }
      resv(reply);
      
    });
  })
export const getClient=()=>client