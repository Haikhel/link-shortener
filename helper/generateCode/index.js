
import crypto from 'crypto'
import {getClient } from '../../redis/index'


export default function generateCode(oldLink) {
  const code = crypto.randomBytes(5).toString('hex')
  const client = getClient()
  if (!client.get(code)) {
    console.log('generete new code')
    return generateCode(oldLink)
  }
  client.set(code,oldLink)
  console.log(code)
  
  return code
}