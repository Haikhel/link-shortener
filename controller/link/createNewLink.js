import config from '../../config'
import create  from '../../db/controller/create'
import dbName from '../../db/const/dbName'
import generateCode from '../../helper/generateCode/index'

export default (oldLink)=>{
  const code = generateCode(oldLink)
  const newLink = `${config.domen}${code}`
  const obj = {
    oldLink,
    newLink,
    code
  }
  create(dbName.shortLink,obj)
  return newLink
}