import { getValueByKey } from '../../redis'

export default async (newLink) =>{
  const arrayStr = newLink.split('/')
  const code =arrayStr[arrayStr.length-1]
  const model =await getValueByKey(code)
  if (!model) {
    return null
  }
  return model
}